Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         |[de](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/01_operating_manual/S3-0040_A1_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/01_operating_manual/S3-0040_A1_Operating%20Manual.pdf)                  |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/02_assembly_drawing/s3-0040_B_ZNB_mm.pdf)                  |
| circuit diagram          | [de](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/03_circuit_diagram/S3-0040-EPLAN-C.pdf)                 |
| maintenance instructions |                  |
| spare parts              |[de](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/05_spare_parts/S3-0040_A1_EVL.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0040-measurement-module/-/raw/main/05_spare_parts/S3-0040_A1_EVL_engl.pdf)                  |

